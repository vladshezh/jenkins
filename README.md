# Все дальнейшие действия производить в терминале на linux ubuntu

## 1. Скачать репозиторий

```
git clone http://gitlab.com/vladshehz/jenkins.git
```

## 2. Установить docker и docker-compose

```
sudo apt update && apt install docker -y && apt install docker-compose -y && apt install make -y
```

## 3. Запустить установку

Зайдите в скачанный репозиторий и напишите команду

```
make up
```

После запуститься установка контейнера с jenkins

## 4. Начало работы с jenkins

Перейдите в браузере по адресу ```127.0.0.1 или localhost```. Откроется подготовительная страница jenkins'а. После потребуется ввести логин и пароль. За логином и паролем обращайтесь к администратору.
